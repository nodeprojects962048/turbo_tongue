import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";
// import { Observable } from "rxjs";

@Injectable()
export class AuthGaurd implements CanActivate{
    constructor(
        private jwtService: JwtService,
        private configService: ConfigService
        ){}
    
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const authHeaders = request.headers.authorization;
        // console.log(authHeaders);
        if(!authHeaders || !authHeaders.startsWith('Bearer ')){
            return false;
        }
        const token = authHeaders.split(' ')[1];
        // console.log(token);
        try{
            const decode = await this.jwtService.verifyAsync(
                token,
                {
                    secret: this.configService.get('JWT_SECRET')
                }
            );
            request.user = decode;
            return true;
        }catch(err){
            return false;
        }
        
    }
}