import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';

@Injectable()
export class AdminRoleGuard implements CanActivate {
  constructor(private jwtService: JwtService, private configService: ConfigService){}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const authHeaders = request.headers.authorization;
    if(!authHeaders || !authHeaders.startsWith('Bearer ')){
      return false;
    }
    const token = authHeaders.split(' ')[1];
    // console.log(token);
    try{
      const decode = await this.jwtService.verifyAsync(
        token,
        {
          secret: this.configService.get('JWT_SECRET')
        }
      );
      // console.log(decode.user_role);
      if(decode.user_role === 'ADMIN'){
        request.user= decode;
        return true;
      }else {
        return false;
      }
    }catch(err){
      return false;
    }

    // return true;
  }
}
