import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { AuthGaurd } from 'src/gaurd/auth.gaurd';

@Module({
    imports: [
        JwtModule.registerAsync({
            useFactory: (configService: ConfigService) => ({
                secret: configService.get('JWT_SECRET'),
                signOptions: {expiresIn: '24h'}
            }),
            inject: [ConfigService],
        })
    ],
    providers: [
        JwtService,
        ConfigService,
        AuthGaurd
    ],
    exports: [
        JwtService,
        ConfigService,
        AuthGaurd
    ]
})
export class GlobalModule {}
