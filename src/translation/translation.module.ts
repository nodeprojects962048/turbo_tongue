import { Module } from '@nestjs/common';
import { TranslationController } from './translation.controller';
import { TranslationService } from './translation.service';
import { GlobalModule } from 'src/global/global.module';
import { CloudinaryUtils } from 'src/utills/cloudinary.util';

@Module({
  imports: [GlobalModule],
  controllers: [TranslationController],
  providers: [TranslationService, CloudinaryUtils]
})
export class TranslationModule {}
