import { Body, Controller, Delete, Get, Param, Patch, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { AdminRoleGuard } from 'src/gaurd/admin-role.guard';
import { AuthGaurd } from 'src/gaurd/auth.gaurd';
import { TranslationService } from './translation.service';
import { DeleteTranslationDto, GetFlashCardTraslationsDto, TranslationDto, UpdateTranslationDto } from 'src/dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { multerConfig } from 'src/utills/multer.util';
import { Multer } from 'multer';

@Controller('translation')
@UseGuards(AuthGaurd)
@UseGuards(AdminRoleGuard)
export class TranslationController {
    constructor( private translationService: TranslationService){}
    @Post()
    @UseInterceptors(FileInterceptor('translation_audio', multerConfig(1 * 1024 * 1024, 'audio')))
    createTranslation(
        @Body() dto: TranslationDto, 
        @UploadedFile() file: Express.Multer.File
    ){
        return this.translationService.createTranslation(dto, file);
        // return {dto, file};
    }

    @Get(':flashcard_id')
    getFlashcardTranslations(@Param() dto: GetFlashCardTraslationsDto){
        return this.translationService.getTranslationsByFlashcardId(dto);
    }


    @Delete(':translation_id')
    deleteTranslation(@Param() dto: DeleteTranslationDto){
        return this.translationService.deleteTranslation(dto);
    }

    @Patch()
    @UseInterceptors(FileInterceptor('translation_audio', multerConfig(1 * 1024 * 1024, 'audio')))
    updateTranslation(
        @Body() dto: UpdateTranslationDto,
        @UploadedFile() file: Express.Multer.File
    ){
        return this.translationService.updateTranslation(dto, file);
    }
}
