import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { DeleteTranslationDto, GetFlashCardTraslationsDto, UpdateTranslationDto } from 'src/dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { CloudinaryUtils } from 'src/utills/cloudinary.util';

@Injectable()
export class TranslationService {
    constructor(private prisma: PrismaService, private cloudinary: CloudinaryUtils) { }

    async createTranslation(dto: any, file: Express.Multer.File) {
        const flashcard_id = parseInt(dto.flashcard_id);
        const translationExists = await this.prisma.translation.findFirst({
            where: {
                translation_language: dto.translation_language,
                flashcard_id: flashcard_id
            }
        })
        if (translationExists) {
            throw new ConflictException('Translation of the same language already exists for this flashcard');
        } else {
            const uploadAudio = await this.cloudinary.uploadAudio(file.path);
            const createdTranslation = await this.prisma.translation.create({
                data: {
                    translation_language: dto.translation_language,
                    translation_text: dto.translation_text,
                    translation_voice_id: uploadAudio.public_id,
                    translation_voice_link: uploadAudio.audio_url,
                    flashcard_id: flashcard_id
                }
            });
            return {
                message: 'Language Created Successfully',
                translation: createdTranslation
            }
        }
    }

    async getTranslationsByFlashcardId(dto: GetFlashCardTraslationsDto) {
        const translations = await this.prisma.translation.findMany({
            where: {
                flashcard_id: dto.flashcard_id
            }
        });
        if (translations.length === 0) {
            throw new NotFoundException('No translations found for the provided flashcard ID');
        }
        return {
            message: 'Success',
            translations
        }
    }

    async deleteTranslation(dto: DeleteTranslationDto) {
        const translation = await this.prisma.translation.findFirst({
            where: {
                translation_id: dto.translation_id
            }
        })
        if (!translation) {
            throw new NotFoundException('No translations found with this ID');
        } else {
            const deleteFile = await this.cloudinary.deleteAudio(translation.translation_voice_id);
            console.log(deleteFile);
            const deleteTranslation = await this.prisma.translation.delete({
                where: {
                    translation_id: dto.translation_id
                }
            })
            return {
                "message": "Translation Deleted Successfully",
                "Deleted Lanugage": deleteTranslation
            }
        }
    }

    async updateTranslation(dto: UpdateTranslationDto, file: Express.Multer.File) {
        const translation = await this.prisma.translation.findFirst({
            where: { translation_id: dto.translation_id },
        });

        if (!translation) {
            throw new NotFoundException('No Translation found with this ID');
        }

        const flashcardId = translation.flashcard_id;
        let translationVoiceId = translation.translation_voice_id;
        let translationVoiceLink = translation.translation_voice_link;
        let translationLang = translation.translation_language;

        if (dto.translation_language) {
            translationLang = dto.translation_language;
            const conflictingTranslation = await this.prisma.translation.findFirst({
                where: {
                    translation_language: dto.translation_language,
                    flashcard_id: flashcardId,
                    NOT: { translation_id: dto.translation_id },
                },
            });

            if (conflictingTranslation) {
                throw new ConflictException(
                    `A translation for language ${dto.translation_language} already exists for this flashcard.`,
                );
            }
        }

        if (file) {
            if (translationVoiceId) {
                await this.cloudinary.deleteAudio(translationVoiceId);
            }

            const uploadedAudio = await this.cloudinary.uploadAudio(file.path);
            translationVoiceId = uploadedAudio.public_id;
            translationVoiceLink = uploadedAudio.audio_url;
        }

        const updatedTranslation = await this.prisma.translation.update({
            where: { translation_id: dto.translation_id },
            data: {
                translation_language: translationLang,
                translation_text: dto.translation_text ? dto.translation_text : translation.translation_text,
                translation_voice_id: translationVoiceId,
                translation_voice_link: translationVoiceLink,
            },
        });

        return {
            success: true,
            message: 'Translation updated successfully',
            updatedTranslation,
        };
    }

}
