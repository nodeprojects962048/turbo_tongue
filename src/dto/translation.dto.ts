import { Type } from "class-transformer";
import { IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";

export class TranslationDto {
    @IsNotEmpty()
    @IsString()
    translation_language: string

    @IsNotEmpty()
    @IsString()
    translation_text: string

    @IsNotEmpty()
    flashcard_id: number
}

export class GetFlashCardTraslationsDto{
    @IsNotEmpty()
    @IsNumber()
    @Type(() => Number)
    flashcard_id: number
}

export class DeleteTranslationDto{
    @IsNotEmpty()
    @IsNumber()
    @Type(()=> Number)
    translation_id: number
}

export class UpdateTranslationDto{
    @IsNotEmpty()
    @IsNumber()
    @Type(()=> Number)
    translation_id: number

    @IsOptional()
    @IsString()
    translation_language: string

    @IsOptional()
    @IsString()
    translation_text: string

    // @IsOptional()
    // @is
    // flashcard_id: number

}