import { IsEmail, IsNotEmpty, IsString } from "class-validator";
import { MatchPassword } from "src/custom_decorators/matchPassword.decorator";

export class SignupDto{
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsString()
    @IsNotEmpty()
    password: string;

    @IsString()
    @IsNotEmpty()
    @MatchPassword('password')
    confirmPassword: string;
}