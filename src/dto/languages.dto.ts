import { Type } from "class-transformer";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class LanguagesDto{
    @IsNotEmpty()
    @IsString()
    lang_code: string

    @IsNotEmpty()
    @IsString()
    lang_name: string
}

export class DeleteLanguageDto{
    @IsNotEmpty()
    @IsNumber()
    @Type(() => Number)
    lang_id: number
}