import { Type } from "class-transformer";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class FlashcardDto{
    @IsNotEmpty()
    @IsString()
    flashcard_name: string

}

export class DeleteFlashCardDto{
    @IsNotEmpty()
    @IsNumber()
    @Type(() => Number)
    flashcard_id: number
}