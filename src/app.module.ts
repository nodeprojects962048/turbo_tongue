import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { PrismaModule } from './prisma/prisma.module';
import { ConfigModule } from '@nestjs/config';
import { FlashcardModule } from './flashcard/flashcard.module';
import { LanguagesModule } from './languages/languages.module';
import { GlobalModule } from './global/global.module';
import { TranslationModule } from './translation/translation.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    AuthModule,
    PrismaModule,
    FlashcardModule,
    LanguagesModule,
    GlobalModule,
    TranslationModule
  ],
})
export class AppModule {}
