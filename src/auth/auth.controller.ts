import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { SigninDto, SignupDto } from 'src/dto';
import { AuthGaurd } from 'src/gaurd/auth.gaurd';

@Controller('auth')
export class AuthController {
    constructor(private readonly authservice:AuthService){}

    @Post('signup')
    signup(@Body() dto: SignupDto){
        // return {dto};
        return this.authservice.signup(dto);
    }

    @Post('signin')
    signin(@Body() dto: SigninDto){
        // return {dto};
        return this.authservice.signIn(dto);
    }

    @Post('admin/signup')
    adminSignup(@Body() dto: SignupDto) {
        return this.authservice.adminSignup(dto);
    }

    @Get('profile')
    @UseGuards(AuthGaurd)
    getProfile(){
        return 'Access granted';
    }
}
