import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { SigninDto, SignupDto } from 'src/dto';
import { PrismaService } from 'src/prisma/prisma.service';
import * as argon from "argon2";
import { PrismaClientKnownRequestError } from '@prisma/client/runtime/library';
import { JwtService } from '@nestjs/jwt';
import { throwError } from 'rxjs';

@Injectable()
export class AuthService {
    constructor(
        private prisma:PrismaService,
        private jwtService: JwtService
        ){}
    
    async signup(dto: SignupDto){
        const hashedPassword = await argon.hash(dto.password);
        try{
            const user = await this.prisma.user.create({
                data: {
                    user_email: dto.email,
                    user_password: hashedPassword
                }
            });
            return {
                message: "Signed Up Successfully",
                user_id: user.user_id
            }

        }catch(err){
            if(err instanceof PrismaClientKnownRequestError){
                if(err.code == 'P2002'){
                   throw new ForbiddenException('Cardentials Taken');
                }
            }else{
                throw err;
            }
        }
    }
    async adminSignup(dto: SignupDto){
        const hashedPassword = await argon.hash(dto.password);
        try{
            const user = await this.prisma.user.create({
                data: {
                    user_email: dto.email,
                    user_password: hashedPassword,
                    user_role: 'ADMIN'
                }
            });
            delete user.user_password;
            return {
                Message: "Admin Registered Successfully",
                user
            }
        }catch(err){
            if(err instanceof PrismaClientKnownRequestError){
                if(err.code == "P2002"){
                    throw new ForbiddenException();
                }
            }else{
                throw err;
            }
        }
    }

    async signIn(dto: SigninDto){
        const user = await this.prisma.user.findUnique({
            where:{
                user_email: dto.email,
            }
        })
        if(!user){
            throw new NotFoundException('User Not Found');
        }
        const isPasswordMatched = await argon.verify(user.user_password, dto.password);
        if(!isPasswordMatched){
            throw new ForbiddenException("Invalid Cardentials");
        }
        delete user.user_password;
        const payload = {user_id: user.user_id, user_role: user.user_role};
        const accessToken = await this.jwtService.signAsync(payload);
        return {
            message: "Signed In Success",
            user,
            accessToken: accessToken
        }
    }
}
