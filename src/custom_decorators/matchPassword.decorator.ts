import { registerDecorator, ValidationOptions, ValidationArguments } from "class-validator";

export function MatchPassword(property: string, ValidationOptions?: ValidationOptions ){
    return function (object: any, propertyName: string){
        registerDecorator({
            name: 'matchPassword',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [property],
            options: ValidationOptions,
            validator: {
                validate(value: any, args: ValidationArguments){
                    const relatedValue = (args.object as any)[args.constraints[0]];
                    return value === relatedValue;
                },
                defaultMessage(args: ValidationArguments) {
                    const relatePropertyName = args.constraints[0];
                    return `${propertyName} must match with ${relatePropertyName}`;
                },
            },
        });
    };
}