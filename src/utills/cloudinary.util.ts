import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import {v2 as cloudinary} from 'cloudinary';

@Injectable()
export class CloudinaryUtils {
    constructor(private readonly configService: ConfigService){
        cloudinary.config({

            cloud_name: this.configService.get('CLOUD_NAME'), 
            api_key: this.configService.get('CLOUDINARY_API_KEY'), 
            api_secret: this.configService.get('CLOUDINARY_API_SECRET'), 
        })
    }
    async uploadAudio (file){
        try{
            const uploadedAudio = await cloudinary.uploader.upload(
                file,
                {
                    resource_type: 'auto',
                }
            )
            return {
                "public_id": uploadedAudio.public_id,
                "audio_url": uploadedAudio.secure_url,
            };
        }catch(err){
            throw new Error(`Cloudinary upload failed: ${err.message}`);
        }
    }

    async deleteAudio(public_id: string){
        try{
            console.log(public_id);
            const deleteFile = await cloudinary.uploader.destroy(public_id, {resource_type: 'video'});
            // console.log(deleteFile.result);
            if(deleteFile.result == 'ok'){
                return {
                    success: true,
                    message: 'File deleted Successfully',
                }
            }
        }catch(err){
            console.error(err);
            throw new Error(`File Deletation Failed: ${err.message}`);
        }
    }
}