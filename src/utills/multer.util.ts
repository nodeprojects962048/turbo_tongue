import { BadRequestException } from "@nestjs/common";
import { MulterOptions } from "@nestjs/platform-express/multer/interfaces/multer-options.interface";
import { diskStorage } from "multer";

export function multerConfig(fileSize: number, fileType: string): MulterOptions{
    return {
        storage: diskStorage({}),
        limits: {fileSize},
        fileFilter: (req, file, callback) => {
            if(!file.mimetype.startsWith(fileType)){
                return callback(new BadRequestException(`Only ${fileType} files are allowd`), false);
            }
            callback(null, true);
        }
    }
}