import { Module } from '@nestjs/common';
import { LanguagesController } from './languages.controller';
import { LanguagesService } from './languages.service';
import { GlobalModule } from 'src/global/global.module';

@Module({
  imports:[
    GlobalModule
  ],
  controllers: [LanguagesController],
  providers: [LanguagesService]
})
export class LanguagesModule {}
