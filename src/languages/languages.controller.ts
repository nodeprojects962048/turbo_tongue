import { Body, Controller, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';
import { LanguagesService } from './languages.service';
import { AuthGaurd } from 'src/gaurd/auth.gaurd';
import { DeleteLanguageDto, LanguagesDto } from 'src/dto/languages.dto';
import { AdminRoleGuard } from 'src/gaurd/admin-role.guard';

@Controller('languages')
@UseGuards(AuthGaurd)
@UseGuards(AdminRoleGuard)
export class LanguagesController {
    constructor(private readonly langServices:LanguagesService ){}
    @Post()
    createLang(@Body() dto: LanguagesDto) {
        return this.langServices.createLanguage(dto);
    }
    
    @Get()
    getLanguages(){
        return this.langServices.getLanguages();
    }

    @Delete(':lang_id')
    deleteLanguage(@Param() dto: DeleteLanguageDto){
        return this.langServices.deleteLanguage(dto);
    }
}
