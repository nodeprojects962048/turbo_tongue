import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime/library';
import { DeleteLanguageDto, LanguagesDto } from 'src/dto/languages.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class LanguagesService {
    constructor(
        private prisma: PrismaService
    ) { }
    async createLanguage(dto: LanguagesDto) {
        try {
            const language = await this.prisma.language.create({
                data: {
                    lang_code: dto.lang_code,
                    lang_name: dto.lang_name
                }
            });
            return {
                message: "Language Created Successfully",
                language_id: language.lang_id,
                language_name: language.lang_name
            }

        } catch (err) {
            if (err instanceof PrismaClientKnownRequestError) {
                if (err.code == 'P2002') {
                    throw new ForbiddenException('Language with this Language Code already exists');
                }
            } else {
                throw err;
            }
        }
    }

    async getLanguages() {
        const languages = await this.prisma.language.findMany();
        if (languages.length === 0) {
            return { message: 'No language found' };
        }
        return languages;

    }

    async deleteLanguage(dto: DeleteLanguageDto) {
        try {
            const deltedLanguage = await this.prisma.language.delete({
                where: { lang_id: dto.lang_id }
            });
            return {
                "message": "language Deleted Successfully",
                "Deleted Lanugage": deltedLanguage
            }
        } catch (err) {
            if (err instanceof PrismaClientKnownRequestError) {
                if (err.code == 'P2025') {
                    throw new NotFoundException('Language Not Found');
                }
            } else {
                throw err;
            }

        }
    }
}
