import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, UseGuards } from '@nestjs/common';
import { FlashcardService } from './flashcard.service';
import { AuthGaurd } from 'src/gaurd/auth.gaurd';
import { AdminRoleGuard } from 'src/gaurd/admin-role.guard';
import { DeleteFlashCardDto, FlashcardDto } from 'src/dto';

@Controller('flashcard')
@UseGuards(AuthGaurd)
@UseGuards(AdminRoleGuard)
export class FlashcardController {
    constructor( private readonly flashCardService: FlashcardService)
    {}
    @Post()
    createFlashcard(@Body() dto: FlashcardDto){
        return this.flashCardService.createFlashcard(dto);
    }

    @Get()
    getFlashCards(){
        return this.flashCardService.getFlashCards();
    }

    @Delete('delete/:flashcard_id')
    deleteFlashCardDto(@Param() dto: DeleteFlashCardDto){
        return this.flashCardService.deleteFlashCard(dto);
    }
}
