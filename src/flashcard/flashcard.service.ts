import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime/library';
import { DeleteFlashCardDto, FlashcardDto } from 'src/dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class FlashcardService {
    constructor(private pirsma: PrismaService) { }
    async createFlashcard(dto: FlashcardDto) {
        try {
            const flashCard = await this.pirsma.flashcard.create({
                data: {
                    flashcard_name: dto.flashcard_name
                }
            });
            return {
                "message": "Flashcard Created Successfully",
                'flashcard_id': flashCard.flashcard_id,
                'flashcard_name': flashCard.flashcard_name
            }
        } catch (err) {
            if (err instanceof PrismaClientKnownRequestError) {
                if (err.code == 'P2002') {
                    throw new ForbiddenException('FlashCard with same name already exists, please use another name.')
                }
            } else {
                throw (err);
            }
        }
    }
    async getFlashCards() {
        const flashCards = await this.pirsma.flashcard.findMany();
        if (flashCards.length === 0) {
            return { "message": "No Flashcard Found" }
        } else {
            return flashCards;
        }
    }

    async deleteFlashCard(dto: DeleteFlashCardDto) {
        try {
            const deletedFlashCard = await this.pirsma.flashcard.delete({
                where: {
                    flashcard_id: dto.flashcard_id,
                }
            });
            return {
                "message": "FlashCard Deleted Successfully",
                'DeletedFlashcard': deletedFlashCard
            }

        } catch (err) {
            if (err instanceof PrismaClientKnownRequestError) {
                if (err.code == 'P2025') {
                    throw new NotFoundException('FlashCard Not Found');
                }
            }
            throw err;
        }
    }
}
