import { Module } from '@nestjs/common';
import { FlashcardController } from './flashcard.controller';
import { FlashcardService } from './flashcard.service';
import { GlobalModule } from 'src/global/global.module';

@Module({
  imports: [
    GlobalModule
  ],
  controllers: [FlashcardController],
  providers: [FlashcardService]
})
export class FlashcardModule {}
