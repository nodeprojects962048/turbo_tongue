-- CreateTable
CREATE TABLE "flashcards" (
    "flashcard_id" SERIAL NOT NULL,
    "flashcard_name" TEXT NOT NULL,

    CONSTRAINT "flashcards_pkey" PRIMARY KEY ("flashcard_id")
);

-- CreateIndex
CREATE UNIQUE INDEX "flashcards_flashcard_name_key" ON "flashcards"("flashcard_name");
