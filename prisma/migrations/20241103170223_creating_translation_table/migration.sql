-- CreateTable
CREATE TABLE "translations" (
    "translation_id" SERIAL NOT NULL,
    "translation_language" TEXT NOT NULL,
    "translation_text" TEXT NOT NULL,
    "translation_voice_id" TEXT NOT NULL,
    "translation_voice_link" TEXT NOT NULL,
    "flashcard_id" INTEGER NOT NULL,

    CONSTRAINT "translations_pkey" PRIMARY KEY ("translation_id")
);
