-- CreateTable
CREATE TABLE "languages" (
    "lang_id" SERIAL NOT NULL,
    "lang_code" TEXT NOT NULL,
    "lang_name" TEXT NOT NULL,

    CONSTRAINT "languages_pkey" PRIMARY KEY ("lang_id")
);
