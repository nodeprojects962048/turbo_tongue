/*
  Warnings:

  - A unique constraint covering the columns `[lang_code]` on the table `languages` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "languages_lang_code_key" ON "languages"("lang_code");
